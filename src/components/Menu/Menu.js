import React from 'react';
import './Menu.css';

const Menu = (props) =>{
  return(
    <div className='Items-list'>
      <button onClick={() => props.onClick(props.name)}>✓</button>
      <h6>{props.name}</h6>
      <p>Price: {props.price} Сом.</p>
    </div>
  )
};

export default Menu